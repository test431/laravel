#### lumen 学习资料，包含以下功能

1. 封装yar服务
2. 私有composer仓库
3. redis 队列
4. apollo 配置中心
5. sentry错误捕捉
6. 数据库查询和使用memcache缓存
7. service provide注册和command使用
8. 打包docker镜像，使用docker-composer部署
9. 使用gitlab cicd 持续打包镜像并更新容器

