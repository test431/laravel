<?php

namespace App\Console\Commands;

use App\Jobs\ExampleJob;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Queue;

class TestCommand extends Command {

    protected $signature = "test_command";

    protected $description = "测试命令";

    public function __construct()
    {
        parent::__construct();
    }

    public function handle(){
        $date = Carbon::now()->addMinutes(1);
        Queue::later($date, new ExampleJob("fang456"));
        echo "execute";
    }
}
