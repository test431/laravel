<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->group(['namespace' => 'Test'], function($router) {
    $router->get('version', 'IndexController@version');
    $router->get('json', 'IndexController@json');
    $router->get('lsuser', 'IndexController@users');
    $router->get('jobs', 'IndexController@jobs');
    $router->get('config', 'IndexController@config');
    $router->get('error', 'IndexController@error');
});
